from model import Registro, sessao
import xlwt

wb = xlwt.Workbook()
ws = wb.add_sheet('Acessos')

ws.write(0, 0, 'Acesso')
ws.write(0, 1, 'Nome')
ws.write(0, 2, 'Id')
ws.write(0, 3, 'URL')
ws.write(0, 4, 'Data')
ws.write(0, 5, 'Hora')

date_format = xlwt.XFStyle()
date_format.num_format_str = 'dd/mm/yyyy'

time_format = xlwt.XFStyle()
time_format.num_format_str = 'hh:mm:ss'

for instance in sessao().query(Registro).order_by(Registro.id):
    ws.write(instance.num + 1, 0, instance.num)
    ws.write(instance.num + 1, 1, instance.name)
    ws.write(instance.num + 1, 2, instance.id)
    ws.write(instance.num + 1, 3, instance.url)
    ws.write(instance.num + 1, 4, instance.data, date_format)
    ws.write(instance.num + 1, 5, instance.hora, time_format)
    print(instance.num, instance.name, instance.id, instance.url,
          instance.data, instance.hora)

wb.save('Acessos.xls')
