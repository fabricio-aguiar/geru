from wsgiref.simple_server import make_server
from pyramid.config import Configurator
from pyramid.session import UnencryptedCookieSessionFactoryConfig
from pyramid.response import Response
from pyramid.renderers import render_to_response
from pyramid.view import view_config
import geru
import json
import ast
from random import randint
from datetime import datetime as ts
from model import Registro, sessao
from sqlalchemy import func

# Carregando as quotes a partir da lib e convertendo em json
desafio = geru.Desafio()
quotes = json.loads(desafio.get_quotes())
quotes = [item for item in quotes['quotes']]


def load_database():
    """
    Carrega os dados do sqlalchemy e converte em dict
    """
    query = sessao().query(Registro).order_by(Registro.num)
    aux = [str(item) for item in query]
    return [ast.literal_eval(item) for item in aux]


def myview(request):
    """
    Realiza o registro de sessão
    """
    session = request.session
    rows = sessao().query(Registro).count()
    last = sessao().query(func.max(Registro.id)) # Pega o ultimo (maior) identificador cadastrado
    last = last.one()[0]
    if rows == 0:
        last = 0

    if not session.new:
        print(session['user'] + ' was in the session')

    else:
        session['id'] = str(last + 1)
        session['user'] = 'Name' + session['id']
        print(session['user'] + ' was not in the session')

    agora = ts.now()
    data = agora.strftime('%Y-%m-%d')
    data = ts.strptime(data, '%Y-%m-%d').date()
    hora = agora.strftime('%H:%M:%S')
    hora = ts.strptime(hora, '%H:%M:%S').time()

    registro = Registro(num=rows, id=int(session['id']), name=session['user'],
                        url=request.current_route_url(), data=data, hora=hora)

    s = sessao()
    s.add(registro)
    s.commit()
    for instance in sessao().query(Registro).order_by(Registro.id):
        print(instance.num, instance.name, instance.id, instance.url,
              instance.data, instance.hora)


def bullet_points(lista):
    """
    Adiciona bullet points em uma lista
    """
    lista = ["<li>" + item + "</li>" for item in lista]
    lista[0] = "<ul>" + lista[0]
    size = len(lista) - 1
    lista[size] = lista[size] + "</ul>"
    points = ''.join(lista)
    return points


@view_config(request_method='GET', context=load_database(), renderer='json')
def list_consultas(context, request):
    """
    Cria endpoints RESTful para visualização das consultas.
    """
    dicio = load_database()
    return render_to_response('json', ["<br> " + str(item) for item in dicio])


def title(request):
    """
    Apresenta página HTML simples contendo título "Desafio Web 1.0"
    """
    myview(request)
    return Response('<body><h1>Desafio Web 1.0</h1></body>')


def bullets(request):
    """
    Apresenta página contendo as frases em bullet points todos os quotes retornados pela API.
    """
    myview(request)
    return Response(bullet_points(quotes))


def quote_number(request):
    """
    Apresenta página contendo o quote retornado pela API correspondente ao <quote_number> ou random <quote_number>
    """
    myview(request)
    numero = '%(numero)s' % request.matchdict
    if numero != "random":
        numero = int(numero)
        quote_to_browser = quotes[numero]
    else:
        numero = randint(0, len(quotes)-1)
        quote_to_browser = "Numero : " + str(numero) + "<br>Quote: " \
            + quotes[numero]
    return Response(quote_to_browser)


if __name__ == '__main__':
    my_session_factory = UnencryptedCookieSessionFactoryConfig('itsaseekreet')
    with Configurator() as config:
        config.add_route('raiz', '/')
        config.add_route('quotes', '/quotes')
        config.add_route('quote_number', '/quotes/{numero}')
        config.add_route('consulta', '/consulta')
        config.add_view(title, route_name='raiz')
        config.add_view(bullets, route_name='quotes')
        config.add_view(quote_number, route_name='quote_number')
        config.add_view(list_consultas, route_name='consulta')
        config.set_session_factory(my_session_factory)
        app = config.make_wsgi_app()
    server = make_server('0.0.0.0', 8000, app)
    server.serve_forever()    