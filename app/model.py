from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.dialects.sqlite import DATE, TIME
from sqlalchemy.orm import sessionmaker

engine = create_engine('sqlite:///registro.db', echo=True)
Session = sessionmaker(bind=engine)


def sessao():
    session = Session()
    return session


Base = declarative_base()


class Registro(Base):
    __tablename__ = "acesso"

    num = Column(Integer, primary_key=True)
    id = Column(Integer)
    name = Column(String)
    url = Column(String)
    data = Column(DATE, nullable=False)
    hora = Column(TIME, nullable=False)

    def __repr__(self):
        return "{ 'consulta': '%d', 'name' : '%s', 'id' : '%d'," \
            "'url' : '%s', 'hora' : '%s', 'data' : '%s' }" % \
            (self.num, self.name, self.id, self.url, self.hora, self.data)


Base.metadata.create_all(engine)