from ..connection import Connection


class Desafio(Connection):

    def get_quotes(self):
        """
        Retorna todas as quotes do Desafio GERU
        
        """

        return self.perform_request('https://1c22eh3aj8.execute-api.us-east-1.amazonaws.com/challenge/quotes')

    def get_quote(self, quote_number=None):
        """
        Retorna uma quote de acordo com seu numero.
        Args:
            quote_number: Identificador da quote
        """
        base_url = "https://1c22eh3aj8.execute-api.us-east-1.amazonaws.com/challenge/quotes/"
        if type(quote_number) == type(1):
            quote_number = str(quote_number)
        

        return self.perform_request(base_url + quote_number)


