from .quotes import Desafio


class QuotesClient(object):
    """
    GERU API client. 

    Voce pode inicializar sua conexao assim:
        desafio = geru.Desafio()

    Consulta API e retorna dicionário python contendo os quotes:
        desafio.get_quotes()
    Consulta API e retorna o quote correspondente:
        desafio.get_quote(quote_number)
    
    """

    def __init__(self):

        self.quote = Desafio()
        