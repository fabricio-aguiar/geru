# Desafio Geru

[Fabricio Aguiar](https://fabricio-aguiar.github.io/)


## Parte 1 - lib

### Instalação

```
python setup.py install
```
### Acesso a API

*import geru*

*desafio = geru.Desafio()*


* **Consulta API e retorna dicionário python contendo os quotes:**
   
    *desafio.get_quotes()*

* **Consulta API e retorna o quote correspondente:**

    *desafio.get_quote(quote_number)*

## Parte 2 - Desafio Web 1.0

Os arquivos do desafio web se encontram na pasta **app** :

*  **geruAPP** - Principal arquivo, gera as visualizações no browser.
*  **model** - Arquivo onde o banco de dados é criado.
*  **Database_to_Excel** - Cria uma planinha, utilizando os dados do sqlalchemy.